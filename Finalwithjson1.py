from flask import Flask, render_template, request, jsonify, json
import sqlite3 as sql
app = Flask(__name__)

@app.route('/')
def home():
   return render_template('student.html')

@app.route('/enternew')
def new_student():
   return render_template('student.html')
@app.route('/list')
def list():
   con = sql.connect("database.db")
   con.row_factory = sql.Row
   
   cur = con.cursor()
   cur.execute("select * from mainsite_server")
   
   rows = cur.fetchall();
   columns = [column[0] for column in cur.description]
   data1 = []
   for row in rows:
      #data.append([x for x in row])
      data1.append(dict(zip(columns, row)))
   return json.dumps(data1)
   #return render_template("list.html",rows = rows)
@app.route('/dialerxn/<string:qsname>')
def specific_server(qsname):
   con = sql.connect("database.db")
   con.row_factory = sql.Row
   cur = con.cursor()
   #cur.execute("SELECT * FROM mainsite_server WHERE name = '%s'", qsname)
   cur.execute("select * from mainsite_server where mainsite_server.name ='" + qsname +"'")
   #query_string = "SELECT * FROM mainsite_server  WHERE name = '{qsname}'".format(qsname=qsname)  
   #cursor.execute(query_string)
   rows = cur.fetchall();
   columns = [column[0] for column in cur.description]
   data2 = []
   for row in rows:
      #data.append([x for x in row])
      data2.append(dict(zip(columns, row)))
   return json.dumps(data2)
   #return render_template("list.html",rows = rows)

@app.route('/script/<string:psname>',methods=['GET','POST'])
def script_server(psname):
   con = sql.connect("database.db")
   con.row_factory = sql.Row
   cur = con.cursor()

   #cur.execute("SELECT * FROM mainsite_server WHERE name = '%s'", qsname)
   cur.execute("SELECT A.server_ip as server_ip, A.server_added_pool as server_added_pool, A.server_gateway as server_gateway,A.connect_ip as connect_ip, A.server_subnet as server_subnet, B.name as name FROM mainsite_server  A , mainsite_datacenter  B WHERE  A .`datacenter.id` = B.id  and A.name='" + psname +"'")
   #query_string = "SELECT * FROM mainsite_server  WHERE name = '{qsname}'".format(qsname=qsname)  
   #cursor.execute(query_string)
   rows = cur.fetchall()
   columns = [column[0] for column in cur.description]
   #return render_template("script.html",rows = rows)
   #rows1 = [ dict(rows) for row in rows ]
   #return json.dumps(data=rows)
   data = []
   for row in rows:
      #data.append([x for x in row])
      data.append(dict(zip(columns, row)))
   return json.dumps(data)


@app.route('/update/<string:sname>/<string:rname>')
def udate_server(sname,rname):
   con = sql.connect("database.db")
   con.row_factory = sql.Row
   cur = con.cursor()
   #cur.execute("SELECT * FROM mainsite_server WHERE name = '%s'", qsname)
   cur.execute("UPDATE mainsite_server SET dc_provided_name = '" + rname +"' WHERE name ='" + sname + "'")
   con.commit()
   con.close()

   con = sql.connect("database.db")
   con.row_factory = sql.Row
   cur = con.cursor()
   #query_string = "SELECT * FROM mainsite_server  WHERE name = '{qsname}'".format(qsname=qsname)  
   #cursor.execute(query_string)
   cur.execute("select * from mainsite_server where mainsite_server.name ='" + sname +"'")
   rows = cur.fetchall();
   return render_template("list.html",rows = rows)


if __name__ == '__main__':
	app.run(host='0.0.0.0')
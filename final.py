from flask import Flask, render_template, request
import sqlite3 as sql
app = Flask(__name__)

@app.route('/')
def home():
   return render_template('student.html')

@app.route('/enternew')
def new_student():
   return render_template('student.html')
@app.route('/list')
def list():
   con = sql.connect("database.db")
   con.row_factory = sql.Row
   
   cur = con.cursor()
   cur.execute("select * from mainsite_server")
   
   rows = cur.fetchall();
   return render_template("list.html",rows = rows)
@app.route('/bilal/<string:qsname>')
def specific_server(qsname):
   con = sql.connect("database.db")
   con.row_factory = sql.Row
   cur = con.cursor()
   #cur.execute("SELECT * FROM mainsite_server WHERE name = '%s'", qsname)
   cur.execute("select * from mainsite_server where mainsite_server.name ='" + qsname +"'")
   #query_string = "SELECT * FROM mainsite_server  WHERE name = '{qsname}'".format(qsname=qsname)  
   #cursor.execute(query_string)
   rows = cur.fetchall();
   return render_template("list.html",rows = rows)

@app.route('/update/<int:id>/<string:rname>')
def udate_server(id,rname):
   con = sql.connect("database.db")
   con.row_factory = sql.Row
   cur = con.cursor()
   #cur.execute("SELECT * FROM mainsite_server WHERE name = '%s'", qsname)
   cur.execute("Update mainsite_server set mainsite_server.name = '" + rname +"' where mainsite_server.id ='" + id + "'")
   #query_string = "SELECT * FROM mainsite_server  WHERE name = '{qsname}'".format(qsname=qsname)  
   #cursor.execute(query_string)
   cur.execute("select * from mainsite_server where mainsite_server.id ='" + id +"'")
   rows = cur.fetchall();
   return render_template("list.html",rows = rows)


if __name__ == '__main__':
	app.run(host='0.0.0.0')
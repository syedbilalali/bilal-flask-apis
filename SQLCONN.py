rom flask import Flask 
from flask.ext.mysqldb import MySQL 

app = Flask(__name__)
app.config['MYSQL_HOST'] = 'sql12.freemysqlhosting.net' #Database-hostname
app.config['MYSQL_USER'] = 'sql12188270' #Database-username
app.config['MYSQL_PASSWORD'] = 'uECp9nXbbP' #Database-password
app.config['MYSQL_DB'] = 'sql12188270' #Database-name
#app.config['MYSQL_PORT'] = '3306'
mysql = MySQL(app)

@app.route('/')
def index():
    cur = mysql.connection.cursor()
    cur.execute('''SELECT data FROM temptab WHERE id = 1''') #select the tuple whose id=1
    rv = cur.fetchall()
    return str(rv)
@app.route('/addone/<string:insert>')
def add(insert):
    cur = mysql.connection.cursor()
    cur.execute('''SELECT MAX(id) FROM temptab''') #select the tuple whose id is highest
    maxid = cur.fetchone() #(10,)
    cur.execute('''INSERT INTO temptab (id, data) VALUES (%s, %s)''', (maxid[0] + 1, insert)) #insert the new record with id+1
    mysql.connection.commit()
    return "Done"

@app.route('/getall')
def getall():
        cur = mysql.connection.cursor()
        cur.execute('''SELECT * FROM temptab''') #select all records from database
        returnvals = cur.fetchall() #((1, "ID1"), (2, "ID2"),...)

        printthis = ""
        for i in returnvals:
            printthis += str(i) + "<br>"

        return printthis

if __name__ == '__main__':
    app.run(host='0.0.0.0')
    #app.run(debug=True)